import numpy as np
import matplotlib.pyplot as plt
# imports
import keras
from keras import utils as np_utils
import tensorflow
from keras import datasets, layers, models
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from matplotlib import pyplot as plt
import joblib
from skimage.transform import resize
from skimage.io import imread
from scipy.ndimage import rotate
from skimage.transform import resize

import tensorflow as tf
from astropy.io import fits
import glob
import random


def main():
    model = Sequential()

    title='saved_models/model_bs-2_epochs-15_bin-cross_Adagrad-0-1'

    print(title)

    ## Binary Crossentropy 
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', 'keras.optimizers.Adadelta()')
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', 'adam')

    ## Categorical Crossentropy
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.Adadelta())
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', 'adam')

    ## Hinge
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.Adadelta())
    #pipeline_tensorflow(model, 2, 15, 'hinge', 'adam')

    ## Squared hinge
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.Adadelta())
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', 'adam')


    ## SGD
    ### Testing lr w/ Binary Crossentropy
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.SGD(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.SGD(learning_rate=0.01))
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.SGD(learning_rate=0.1))

    ### Categorical Crossentropy 
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.SGD(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.SGD(learning_rate=0.01))
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.SGD(learning_rate=0.1))


    ### Hinge 
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.SGD(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.SGD(learning_rate=0.01))
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.SGD(learning_rate=0.1))

    ### Hinge Squared
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.SGD(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.SGD(learning_rate=0.01))
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.SGD(learning_rate=0.1))
    ######################################################################################################

    ## RMSprop
    ### Testing lr w/ Binary Crossentropy
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.RMSprop(learning_rate=0.0001))
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.RMSprop(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.RMSprop(learning_rate=0.01))

    ### Categorical Crossentropy 
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.RMSprop(learning_rate=0.0001))
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.RMSprop(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.RMSprop(learning_rate=0.01))


    ### Hinge 
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.RMSprop(learning_rate=0.0001))
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.RMSprop(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.RMSprop(learning_rate=0.01))

    ### Hinge Squared
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.RMSprop(learning_rate=0.0001))
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.RMSprop(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.RMSprop(learning_rate=0.01))
    ######################################################################################################

    ## Adagrad
    ### Testing lr w/ Binary Crossentropy
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.Adagrad(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.Adagrad(learning_rate=0.01))
    pipeline_tensorflow(model, 2, 15, 'binary_crossentropy', keras.optimizers.Adagrad(learning_rate=0.1))

    ### Categorical Crossentropy 
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.Adagrad(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.Adagrad(learning_rate=0.01))
    #pipeline_tensorflow(model, 2, 15, 'categorical_crossentropy', keras.optimizers.Adagrad(learning_rate=0.1))


    ### Hinge 
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.Adagrad(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.Adagrad(learning_rate=0.01))
    #pipeline_tensorflow(model, 2, 15, 'hinge', keras.optimizers.Adagrad(learning_rate=0.1))

    ### Hinge Squared
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.Adagrad(learning_rate=0.001))
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.Adagrad(learning_rate=0.01))
    #pipeline_tensorflow(model, 2, 15, 'squared_hinge', keras.optimizers.Adagrad(learning_rate=0.1))

    ####################################################################################################
    model.save(title)
    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'], run_eagerly=True)
    #model = tf.keras.models.load_model('saved_models/model_bs-2_epochs-15_binary-cross_adadelta')
    #validate(model, './val_dataset.npy')
    #predict_fits('./mine/fits/4', model)
    print(title)


def validate(model, path_dir):
    val_data = np.load(path_dir, allow_pickle=True)
    val_data = [fits for fits in val_data]
    val_data = np.array(val_data)
    predictions = model.predict(val_data)
    print(predictions)
    class_pred = predictions.argmax(axis=-1)
    print(class_pred)

#Lambda function for model.predict
predict = lambda model, fits: model.predict(np.array([fits])).argmax(axis=-1)[0]

def predict_fits(file_path, model):
    fits_files_data = [(fits.getdata(file).squeeze(), file) for file in glob.glob(file_path + '/*.fits')]
    fits_files_data = [(crop_around_middle_50x50_percent(data), name)  for (data, name) in fits_files_data if data.shape[0] > 500 and data.shape[1] > 500]
    fits_files_data = [(crop_around_max_value_400x400(data), name) for (data, name) in fits_files_data]
    fits_files_data = [((data[150:250, 150:250]), name) for (data, name) in fits_files_data if data.shape == (400, 400)]
    fits_files_data = [(predict(model, data), data, name) for (data, name) in fits_files_data]

    # zscale = ZScaleInterval(contrast=0.25, nsamples=1)

    for (pos, data, name) in fits_files_data:
        if pos == 1:
            plt.figure(frameon=False)
            plt.imshow(data, origin='lower',
                           cmap='CMRmap_r', aspect='auto')
            plt.title(name)
            plt.show()


#    for fits, probs, label in zip(val_data, predictions, predictions.argmax(axis=-1)):
#        #print(np.argmax(p['probabilities']))
#        # plt.imshow(image.reshape(28, 28), cmap=plt.cm.binary)
#        # plt.show()
#        index=index+1
#
#        print(f'Image number: {index}')
#        print(f'Fits data: {fits}')
#        print(f'Predicted class: {label}')
#        print(f'')
#        print(f'')




def load_to_X_y():

    fits_pos = np.load('./pos_dataset.npy', allow_pickle=True)

    fits_pos = ([linear_transformation(fits)for fits in fits_pos if fits.shape == (400, 400)])
    fits_pos = np.array(fits_pos)

    print(fits_pos.shape)
    print(type(fits_pos))
    
    fits_neg = np.load('neg_dataset.npy', allow_pickle=True)
    fits_neg = ([linear_transformation(fits)for fits in fits_neg if fits.shape == (400, 400)])
    fits_neg = np.array(fits_neg)

    print(fits_neg.shape)
    print(type(fits_neg))


    y = [0] * len(fits_neg) + [1] * len(fits_pos)
    X = np.concatenate((fits_neg, fits_pos), axis=0)

    return X, y

def pipeline_tensorflow(model, batch_size, epochs, loss, optimizer):

    #-------------------Importing data-------------------#

    X, y = load_to_X_y()

    #-------------------Split data-------------------#

    X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.2, random_state=42)

    #-------------------Reshape to tensor-------------------#

    X_train = np.array([tf.convert_to_tensor(fits) for fits in X_train])
    X_test  = np.array([tf.convert_to_tensor(fits) for fits in X_test])

    y_train = np.array([tf.convert_to_tensor(fits) for fits in y_train])
    y_test  = np.array([tf.convert_to_tensor(fits) for fits in y_test])

    #-------------------Convert class vectors to binary class matrices-------------------#

    y_train = np_utils.to_categorical(y_train, 2)
    y_test  = np_utils.to_categorical(y_test, 2)

    #-------------------Create model-------------------#x
    #model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                        activation='relu',
                        input_shape=(100, 100, 1)))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, activation='softmax'))


    #-------------------Compile model-------------------#

    model.summary()

    model.compile(loss=loss,
                    optimizer=optimizer,
                    #optimizer=keras.optimizers.Adam(),
                    metrics=['accuracy'])
    
#        model.compile(loss=keras.losses.categorical_crossentropy,
#                    optimizer=keras.optimizers.Adadelta(),
#                    #optimizer=keras.optimizers.Adam(),
#                    metrics=['accuracy'])

    fit_info = model.fit(X_train, y_train,
                         batch_size=batch_size,
                         epochs=epochs,
                         verbose=1,
                         validation_data=(X_test, y_test))
    
    print(model.evaluate(X_test, y_test, verbose=3))
    y_pred = model.predict(X_test)


def linear_transformation(fits):
    ret_image = fits

    random_resize = random.randint(150, 350)
    ret_image = resize(ret_image, (random_resize, random_resize))

    ret_image = rotate(ret_image, random.randint(0, 360), reshape=False)

    if random.getrandbits(1):
        ret_image = np.fliplr(ret_image)
    if random.getrandbits(1):
        ret_image = np.flipud(ret_image)

    (lwr_bound, upr_bound) = int(random_resize/2) - 55, int(random_resize/2) - 45
    x = random.randint(lwr_bound, upr_bound)
    y = random.randint(lwr_bound, upr_bound)

    return ret_image[x:x+100, y:y+100]

def init_fits_files_from_folder(file_path):
    fits_files_data = [fits.getdata(file).squeeze() for file in glob.glob(file_path + '/*.fits')]
    print(len(fits_files_data))
    fits_files_data = [crop_around_middle_50x50_percent(file) for file in fits_files_data if file.shape[0] > 500 and file.shape[1] > 500]
    return np.array([crop_around_max_value_400x400(file) for file in fits_files_data])

def crop_around_middle_50x50_percent(fits): return fits[(int(fits.shape[0]*.25)):(int(fits.shape[0]*.75)),
                                                        (int(fits.shape[1]*.25)):(int(fits.shape[1]*.75))]

def crop_around_max_value_400x400(fits):
    (x, y) = np.unravel_index(np.argmax(fits, axis=None), fits.shape)
    return fits[x-200:x+200, y-200:y+200]

if __name__ == '__main__':
    main()

