import numpy as np
from astropy.io import fits
import glob




def init_fits_files_from_folder(file_path):
    fits_files_data = [fits.getdata(file).squeeze() for file in glob.glob(file_path + '/*.fits')]
    fits_files_data = [crop_around_middle_50x50_percent(file) for file in fits_files_data if file.shape[0] > 500 and file.shape[1] > 500]
    return np.array([crop_around_max_value_400x400(file) for file in fits_files_data])

def crop_around_middle_50x50_percent(fits):
    return fits[(int(fits.shape[0]*.25)):(int(fits.shape[0]*.75)), (int(fits.shape[1]*.25)):(int(fits.shape[1]*.75))]


def crop_around_max_value_400x400(fits):
    (x, y) = np.unravel_index(np.argmax(fits, axis=None), fits.shape)
    return fits[x-200:x+200, y-200:y+200]

data = [fits.getdata(file).squeeze() for file in glob.glob('./outputs/under_50/*')]
np.save('under_50.npy', data, allow_pickle=True)


#data = [init_fits_files_from_folder('outputs')]
#np.save('val.npy', data, allow_pickle=True)

 # data = [fits.getdata(file).squeeze() for file in glob.glob('./data/cnn/negs/*.fits')]
# np.save('./data/fits/neg_dataset.npy', np.array(data), allow_pickle=True)

